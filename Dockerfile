FROM quay.io/loryza/adj-stack:nersc

RUN mkdir -p /tmp/src
RUN mkdir -p /mnt/xetta/src
RUN mkdir -p /mnt/xetta/host

COPY libadjoint /tmp/src/libadjoint 
COPY dolfin-adjoint /tmp/src/dolfin-adjoint

RUN cd /tmp/src/libadjoint && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_INSTALL_PREFIX=$XETTA \
     -Wno-dev \
     -DCMAKE_PREFIX_PATH=$XETTA \
     -DCMAKE_BUILD_TYPE=Release .. && \
     make install

RUN cd /tmp/src/dolfin-adjoint && \
    $XETTA/bin/python setup.py install --prefix=$XETTA

ENV PYTHONPATH=$XETTA/lib/python2.7/site-packages
ENV LD_LIBRARY_PATH=/mnt/xetta/host/libs:/mnt/xetta/lib
WORKDIR /mnt/xetta/host
